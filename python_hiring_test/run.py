"""Main script for generating output.csv."""

import csv
import math
import time

# Create two dics for stat analysis by reading csv file
##----------------------------------------------------------------------------------------------------------------------------------------------
# 1. hitter_Dic for Hitter including HitterId and HitterTeamId
#    hitter_Dic:
#               key=HitterId-split or HitterTeamId-split
#               value=:['HitterId' or 'HitterTeamId',Split,'PA', 'AB', 'H', '2B', '3B', 'HR', 'TB', 'BB', 'SF', 'HBP']}
#               e.g.{'424825-RHP':['HitterId','vs RHP',1,1,0,0,0,0,0,0,0,0]} or{'114-RHP':['HitterTeamId','vs RHP',1,1,0,0,0,0,0,0,0,0]}
# 2. pitcher_Dic Pitcher including PitcherId and PitcherTeamId
#    pitcher_Dic:
#               key=PitcherId-split or PitcherTeamId-split
#               value=['PitcherId' or 'PitcherTeamId',Split,'PA', 'AB', 'H', '2B', '3B', 'HR', 'TB', 'BB', 'SF', 'HBP']}
#               e.g.{'448306-LHH':['PitcherId','vs LHH',1,1,0,0,0,0,0,0,0,0]},or {'145-LHH':['PitcherTeamId','vs LHH',1,1,0,0,0,0,0,0,0,0]}
##----------------------------------------------------------------------------------------------------------------------------------------------
def create_Dics_ByReading_csv(file_path):
    read_file=open(file_path,'r')
    # title line
    # title:['GameId', 'PitcherId', 'HitterId', 'PitcherSide', 'HitterSide', 'PrimaryEvent', 'PitcherTeamId', 'HitterTeamId', 'PA', 'AB', 'H', '2B', '3B', 'HR', 'TB', 'BB', 'SF', 'HBP']
    # skip title line
    read_file.readline()
    # read all data from csv file
    data_List=read_file.readlines()
    read_file.close()
    # define two dics
    pitcher_Dic={}
    hitter_Dic={}
    # go through data_list for filling out two dics
    for data_Line in data_List:
        split_Data_Line=data_Line.strip('\n').split(',')
        PitcherId=split_Data_Line[1]
        HitterId=split_Data_Line[2]
        PitcherSide=split_Data_Line[3]
        HitterSide=split_Data_Line[4]
        PitcherTeamId=split_Data_Line[6]
        HitterTeamId=split_Data_Line[7]
        value_List=split_Data_Line[8:]
        pitcher_Vs='vs '+HitterSide+'HH'
        hitter_Vs='vs '+PitcherSide+'HP'
        # fill out pitcher_Dic
        fillOut_dic(pitcher_Dic,PitcherId,value_List,pitcher_Vs,'PitcherId')
        fillOut_dic(pitcher_Dic,PitcherTeamId,value_List,pitcher_Vs,'PitcherTeamId')
        # fill out hitter_Dic
        fillOut_dic(hitter_Dic,HitterId,value_List,hitter_Vs,'HitterId')
        fillOut_dic(hitter_Dic,HitterTeamId,value_List,hitter_Vs,'HitterTeamId')
    return hitter_Dic,pitcher_Dic

# fill out dic
def fillOut_dic(dic,playerId,value_List,Split,subject):
    playerSide=Split.split(' ')[1]
    key=playerId+'-'+playerSide
    if(key not in dic.keys()):
        dic[key]=[subject,Split]+value_List
    else:
        get_Value_List=dic.get(key)
        get_Score_List=get_Value_List[2:]
        for i in range(len(get_Score_List)):
            score1=float(get_Score_List[i])
            score2=float(value_List[i])
            get_Score_List[i]=score1+score2
        dic[key]=[subject,Split]+get_Score_List
        
# fill out statistic data table with dic
def fillOut_Data_Table(data_Table,dic):
    # table_Col_Title=['SubjectId','Stat','Split','Subject','Value']
    for key in dic.keys():
        # value_List: ['subject','split']+score_List
        value_List=dic.get(key)
        subject=value_List[0]
        split=value_List[1]
        score_List=value_List[2:]
        # key=playerId-playerside, e.g. key='145-LHH'
        subjectId=int(key.split('-')[0])
        metic_dic=metric_Calculation(score_List)
        if(len(metic_dic)>0):
            for stat in metic_dic.keys():
                data_Table_row=[subjectId,stat,split,subject,metic_dic.get(stat)]
                data_Table.append(data_Table_row)

#------------------------------------------------------
# input: two dics
# output: stat data table that meets combinations.txt
#---------------------------------------------------------
def statistic_Analysis(hitter_Dic,pitcher_Dic):
    # table_Col_Title=['SubjectId','Stat','Split','Subject','Value']
    data_Table=[]
    # create static data stable with two dics
    fillOut_Data_Table(data_Table,hitter_Dic)
    fillOut_Data_Table(data_Table,pitcher_Dic)
    return data_Table

#-----------------------------------
# metric
# 1. AVG=H/AB
# 2. OBP=(H+BB+HBP)/(AB+BB+HBP+SF)
# 3. SLG=TB/AB
# 4. OPS=OBP+SLG
# input: score_List:['PA', 'AB', 'H', '2B', '3B', 'HR', 'TB', 'BB', 'SF', 'HBP']
# output: metric dic:{'AVG':value,'OBP':value,'SLG':value,'OPS':value}
#----------------------------------
def metric_Calculation(score_List):
    H=float(score_List[2])
    AB=float(score_List[1])
    BB=float(score_List[7])
    HBP=float(score_List[9])
    SF=float(score_List[8])
    TB=float(score_List[6])
    PA=float(score_List[0])
    dic={}
    ## calculate the metric if PA>25
    if(PA>=25):
        AVG=H/AB
        OBP=(H+BB+HBP)/(AB+BB+HBP+SF)
        SLG=TB/AB
        OPS=(OBP+SLG)
        dic['AVG']=round(AVG,3)
        dic['OBP']=round(OBP,3)
        dic['SLG']=round(SLG,3)
        dic['OPS']=round(OPS,3)
    return dic
# write data_Table to csv file
def write_DataTable_To_csvFile(data_Table,file_Path):
    outfile=open(file_Path,'w',newline='')
    spawriter=csv.writer(outfile,delimiter=',',quoting=csv.QUOTE_MINIMAL)
    title_Line=['SubjectId','Stat','Split','Subject','Value']
    spawriter.writerow(title_Line)
    for data_Row in data_Table:
        spawriter.writerow(data_Row)
    outfile.close()
    
def main():
    # start time count
    start_time=time.time()
    # raw data path
    file_path='./data/raw/pitchdata.csv'
    # get two dics
    hitter_Dic,pitcher_Dic=create_Dics_ByReading_csv(file_path)
    # get static data_Table
    data_Table=statistic_Analysis(hitter_Dic,pitcher_Dic)
    # sort data table with first columns
    data_Table.sort(key=lambda x:(x[0],x[1],x[2],x[3]))
    # write statistic data_Table into csv file
    output_Path='./data/processed/output.csv'
    write_DataTable_To_csvFile(data_Table,output_Path)
    print("Well Done!")
    # end time count
    end_time=time.time()
    elapsed_time=end_time-start_time
    print("Elapsed Time: "+str(elapsed_time)+' seconds')

if __name__ == '__main__':
    main()
